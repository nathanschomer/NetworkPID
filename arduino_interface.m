cleanup();
clc; clear; close all;

%%%%%%%%%%%%%%%%%% CONFIGURATION  %%%%%%%%%%%%%%%%%%%
% system location of Arduino
usb_port = '/dev/ttyACM0'; 

% IP address/ port of controller computer
ip_addr  = 'localhost';
ip_port  = 30000;

% TODO: setpoint could be initialized to better value
setpoint = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[arduino, tcp_pipe] = setup(usb_port, ip_addr, ip_port);

while true
    tic;
    val = arduino_update(arduino, setpoint);
    setpoint = controller_update(tcp_pipe, val);
    fprintf("Sensor = %d, Setpoint = %d, Time = %f\n", val, setpoint, toc);
    
    past_values = running_plot(val, past_values);
end


% perform necessary setup
function [arduino, tcp_pipe] = setup(usb, ip, port)

    % open serial connection with arduino
    fprintf('Initializing Arduino... ');
    arduino = serial(usb);
    set(arduino,'DataBits',8); % 2 bytes
    set(arduino,'StopBits',1);
    set(arduino,'BaudRate',115200); % max baud rate of Arduino
    set(arduino,'Parity','none');
    fopen(arduino);
    fprintf('done!\n');
    
    % open network connection with other matlab instance
    fprintf('Opening network connection to controller... ');
    tcp_pipe = tcpip(ip, port, 'NetworkRole', 'client' );
    fopen(tcp_pipe);
    fprintf('done!');

    % scolling plot setup
    plot_length = 25;
    past_values = ones(1, plot_length)*100;
    figure;
    drawnow;

end


% Send current setpoint to Arduino and get new sensor reading
function sensorVal = arduino_update(arduino, setpoint)
    
    fwrite(arduino, setpoint);
    sensorVal = fread(arduino, 1);
    
end


% Send curr sensor value and get updated control value
function new_setpoint = controller_update(tcp_pipe, currVal)
    
    fwrite(tcp_pipe, dec2hex(currVal, 4));  
    new_setpoint = ascii2num(fread(tcp_pipe, 4));
    
end


% Converts an ASCII value to an integer
function num = ascii2num(ascii)
    num = hex2dec(char(reshape(ascii, [1, 4])));
end


% TODO: doesn't seem to actually work...
%   this is supposed to avoid having to restart
%   MATLAB after forgetting to close your serial conn
function ret = cleanup()
    
    if exist('arduino', 'var') == 1
        fclose(arduino);
    end

    if exist('tcp_pipe', 'var') == 1
        fclose(tcp_pipe);
    end
    
    ret = 0;

end


% Update rolling plot of controller output
function past_values = running_plot(value, past_values)
    
    past_values = circshift(past_values, [0, -1]);
    past_values(end) = value;
    plot(past_values);
    hold off;
    
    title('Photoresistor Voltage vs. Time');
    xlabel('Time');
    ylabel('Voltage')
    
    ylim([100,200]);
    xlim([0, size(past_values,2)*2]);
    
    grid on;
    set(gca, 'XTickLabel', []);
    drawnow;

end
