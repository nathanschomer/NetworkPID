
#define BAUD_RATE 115200

int tmp = 0; 
int msb;

void setup() {
  
  // setup serial conn
  Serial.begin(BAUD_RATE);
  while (!Serial) {}

  // initalize port B
  DDRB|=B00111111;
  PORTB=00100000;

}

void loop() {
  
  //Read photo-resistor voltage and send to computer
  // (genrally between 100 & 200)
  tmp = analogRead(A0);
  Serial.write(tmp/4);
  
  // Block until a new serial msg is available
  while (Serial.available() == 0) {}
  
  // get new setpoint and write to PORTB
  msb = Serial.read();
  PORTB = msb>>2;
  
  //B=xx000000 sensor=200
  //B=xx111111 sensor=100
}
