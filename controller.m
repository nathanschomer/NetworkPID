cleanup();
clc; clear; close all;

%%%%%%%%%%%%%%%%%% CONFIGURATION  %%%%%%%%%%%%%%%%%%%
ip_addr  = 'localhost';
ip_port  = 30000;

% PID Vals
setpoint = 64;
P = .5;
I = 0;
D = 0;
antiWindThresh = 0;
scale = -1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

deriv = 0;
integ = 0;
newout = setpoint;
err = 0;

tcp_pipe = setup(ip_addr, ip_port);

while true
    
    % get updated sensor value 
    sensorVal = ascii2num(fread(tcp_pipe, 4));
    fprintf('Received: %d\n', sensorVal);
    sensorVal = double(sensorVal-100)*2.56;
    
    prev_err = err;
    err = setpoint - sensorVal;
    
    integ = integ + err;
    if abs(integ) > antiWindThresh
        integ = antiWindThresh*(integ/abs(integ));
    end
        
    deriv = err - prev_err;
    
    update = scale*(P*err + I*integ + D*deriv);
    newout = newout + update;

    % send updated setpoint back
    fwrite(tcp_pipe, dec2hex(uint8(newout), 4));
    
end


% peform initial setup
function tcp_pipe = setup(ip, port)
    
    % open network connection with other matlab instance
    fprintf('Opening network connection to controller... \n');
    tcp_pipe = tcpip(ip, port, 'NetworkRole', 'server' );
    fopen(tcp_pipe);
    fprintf('done!');
    
end


% Converts an ASCII value to an integer
function num = ascii2num(ascii)
    num = hex2dec(char(reshape(ascii, [1, 4])));
end


% TODO: doesn't seem to actually work...
%   this is supposed to avoid having to restart
%   MATLAB after forgetting to close your TCP conn
function ret = cleanup()

    if exist('tcp_pipe', 'var')
        fclose(tcp_pipe);
    end
    
    ret = 0;
end
